<?php
/**
 * @file
 * stonehenge_base.features.inc
 */

/**
 * Implements hook_node_info().
 */
function stonehenge_base_node_info() {
  $items = array(
    'task' => array(
      'name' => t('Task'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'user_story' => array(
      'name' => t('User story'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
