<?php
/**
 * @file
 * stonehenge_base.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function stonehenge_base_taxonomy_default_vocabularies() {
  return array(
    'sprints' => array(
      'name' => 'Sprints',
      'machine_name' => 'sprints',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
  );
}
