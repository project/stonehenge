(function ($) {
  Drupal.behaviors.stonehenge_operations = {
    attach: function (context, settings) {
	  // Let's close the operation box when we click outside it
	  $("body").click(function(event) {
    if ( !$(event.target).hasClass('moderation-form')) {
         $(".moderation-form").replaceWith('<div id="moderation-form-closed" class="moderation-form-closed"></div>');
    }
});
}
  }}(jQuery));
	


