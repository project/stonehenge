<?php

/**
 * A handler to provide a brain forum moderation field
 *
 * @ingroup views_field_handlers
 */
class stonehenge_operations_handler_field_operation_form extends views_handler_field_custom {
  function query() {
    // do nothing -- to override the parent query.
  }

  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    // Remove the checkboxes and rewrite text field
    unset($form['alter']['alter_text']);
    unset($form['alter']['text']['#dependency']);
    unset($form['alter']['text']['#process']);
    unset($form['alter']['help']['#dependency']);
    unset($form['alter']['help']['#process']);
	  unset($form['text']);
  }

  function render($values) {
  //return the form that outputs the moderation link
 $output = stonehenge_operations_form($values); 
  return $output;
  }
}
